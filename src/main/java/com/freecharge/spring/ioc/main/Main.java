package com.freecharge.spring.ioc.main;

import com.freecharge.spring.ioc.Address;
import com.freecharge.spring.ioc.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        //core container
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Employee emp1 = context.getBean("emp1", Employee.class);
        System.out.println(emp1.hashCode());
      /*  Address officeaddress = context.getBean("officeaddress", Address.class);
        System.out.println(officeaddress);*/

        Employee emp2 = context.getBean("emp1", Employee.class);
        System.out.println(emp2.hashCode());

        ((ConfigurableApplicationContext)context).close();
    }
}
